//
//  ViewController.swift
//  RateMyCourse
//
//  Created by Dan Finn on 4/6/17.
//  Copyright © 2017 Dan Finn. All rights reserved.
//

import UIKit
import AWSDynamoDB
import AWSCore
import AWSCognito


class ViewController: UIViewController {
   
    override func viewDidLoad() {
        
        let dbAccessor = DBManager(poolID: "us-east-1:63f21831-90a5-433e-bcee-4ece294731bd")
        
        if let fetchedCourse = dbAccessor.getCourse(courseCode: "L90"){
            print(fetchedCourse.Title)
        }
        
        if let otherFetchedCourse = dbAccessor.getCourse(courseCode: "E81 330S"){
        print(otherFetchedCourse.Title)
        }
        
        let results = dbAccessor.searchCourses(title: "Chemistry")
        for course in results {
            print(course.Title)
        }
        
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

